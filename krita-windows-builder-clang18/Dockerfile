#syntax=docker/dockerfile:1.5-labs
#escape=`
FROM mcr.microsoft.com/windows/server:ltsc2022

LABEL org.opencontainers.image.title="Windows Image for Krita builds"
LABEL org.opencontainers.image.authors="dimula73@gmail.com"

ENV chocolateyUseWindowsCompression=false

# Install Chocolatey
# This must be done before switching over to Powershell as our shell because Chocolatey drops/loads a helper module into Powershell
# This helper is located in a temporary directory, which means our cleanup command fails due to Powershell holding the file open
RUN `
    powershell -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'));" && `
    powershell -Command "Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose -ErrorAction SilentlyContinue"

# Switch to Powershell from here on now that Chocolatey is installed
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# Install chocolatey, then use it to install Git, Python 2 and 3, 7zip and Powershell Core
RUN `
    choco feature disable --name=showDownloadProgress; `
    choco install -y git python310 ccache ninja 7zip powershell-core cmake windows-sdk-10-version-2104-all; `
    Remove-Item @( 'C:\*Recycle.Bin\S-*' ) -Force -Recurse -Verbose; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

# Make sure Git does not attempt to use symlinks as they don't work well with CMake and co
RUN git config --system core.symlinks false

# Install a couple of Python bindings needed by the CI Tooling
RUN pip install lxml pyyaml python-gitlab packaging; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose;

WORKDIR 'c:\tools\'

RUN $url = 'https://github.com/mstorsjo/llvm-mingw/releases/download/20240619/llvm-mingw-20240619-ucrt-x86_64.zip'; `
    $expectedHash = '2ca9a1781eafd59f3024b50f6cf11fb3'; `
    $archiveName = $(Split-Path $url -Leaf); `
    curl.exe -f -SL --no-progress-meter $url -O; `
    if ($LASTEXITCODE -ne 0) {throw \"Failed to download $archiveName. Exit code is $LASTEXITCODE\"}; `
    $hash = $(Get-FileHash -Algorithm MD5 -Path $archiveName).Hash; `
    if ($hash -ine $expectedHash) { throw \"Failed to verify checksum, real: $hash, expected: $expectedHash\"}; `
    7z x $archiveName; `
    if ($LASTEXITCODE -ne 0) {throw \"Failed to unpack $archiveName. Exit code is $LASTEXITCODE\"}; `
    Remove-Item $archiveName

# normal adjusting of PATH using ENV clause doesn't work on Windows, so we need to update
# it using the PowerShell interface
RUN $extraToolsPath = 'c:\Program Files\CMake\bin;c:\tools\llvm-mingw-20240619-ucrt-x86_64\bin;c:\tools\llvm-mingw-20240619-ucrt-x86_64\x86_64-w64-mingw32\bin'; `
    $newPath = ('{0};{1}' -f $extraToolsPath, $env:PATH); `
    [Environment]::SetEnvironmentVariable('PATH', $newPath, [EnvironmentVariableTarget]::Machine)
